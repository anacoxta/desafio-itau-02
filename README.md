## Desafio Itaú 02
# Consolidação de dados usando framework

Esse é o desafio 2 de 3, em que a **habilidade com frameworks Javascript** e com **requisições *http*** é avaliada. Para isso é necessário consumir uma API onde estão os lançamentos de um cartão de credito dentro de um mesmo ano. Instruções de consumo da API [neste link](https://gitlab.com/desafio3/readme-api).   
&nbsp;      
      
### **Links de cada um dos desafios:**

### **Links de cada um dos desafios:**

* DESAFIO 01 = HTML + BOOTSTRAP + JAVASCRIPT VANILLA
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-01
  * Deploy: https://desafio-itau-01.netlify.com/
* **DESAFIO 2 (este) = REACT + BULMA CSS + STYLED-COMPONENTS**
  * **Repositório: https://gitlab.com/anacoxta/desafio-itau-02**
  * **Deploy: https://desafio-itau-02.netlify.com**
* DESAFIO 3 = REACT + MATERIAL-UI + STYLED-COMPONENTS
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-03 _(work in progress)_
  * Deploy: https://desafio-itau-03.netlify.com/ _(work in progress)_      
&nbsp;   


      
### Cenário Proposto
*Neste projeto temos uma API que retorna os lançamentos de um cartão de forma desorganizada.
A necessidade do nosso projeto é criar o front, usando seu framework de preferência. A página criada pode ser similar a do desafio 1, deve conter uma lista de gastos gerais e uma lista de gastos consolidados por mês.*   
&nbsp;      
      
### Requisitos

- [x] O framework utilizado deve ser um framework de Javascript;
- [x] No html deve existir duas tabelas:
  - [x] tabela geral: **Origem | Categoria | Valor Gasto | Mês**;
  - [x] tabela consolidada **Mês | Total Gasto**;
- [x] Os campos Mês e Categoria em ambas as tabelas devem ser exibidos escrito;
- [x] Os dados podem ser apresentados em formato de tabela na mesma página html ou em outra paágina dêsde que seja possível navegar para ela, pela página principal;
- [x] Só devem ser exibidos na tabela consolidada os meses em que houveram lançamentos;
- [x] Usar um framework de CSS (como bootstrap) é recomendado **mas não é obrigatório**.
&nbsp;      
> **Importante: para otimizar a exibição dos dados da Tabela Geral em telas menores, abaixo do breakpoint de 480px os meses são exibidos "encurtados".**    
> *Exemplo: "Jan" em vez de "Janeiro".*   

&nbsp;      
&nbsp;      
      
     
      
# Instruções

Para rodar o código localmente...
1) [Clone](https://help.github.com/pt/github/creating-cloning-and-archiving-repositories/cloning-a-repository) este repositório em sua máquina
2) Abra a linha de comando e acesse (através dela) a pasta do repositório local
3) Use o comando **`npm install`** para instalar as dependências localmente
4) Use o comando **`npm start`** para rodar o aplicativo em modo de desenvolvimento, acessível através do endereço http://localhost:3000 em seu navegador...     
  ...ou use **`npm run build`** para "empacotar" o aplicativo para produção (na pasta _build_).   
&nbsp;   
&nbsp;   


### **Observação**:
Os requisitos mínimos para rodar este projeto são:
* Ter [Node.js](https://nodejs.org/en/download/) instalado localmente na máquina
* Um navegador de sua preferência