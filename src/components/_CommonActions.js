// Replace month number with month name
// export const REPLACEMONTHNAME = (num) => {
//   switch (num) {
//     case 1:
//       return "Janeiro";
//     case 2:
//       return 'Fevereiro';
//     case 3:
//       return 'Março';
//     case 4:
//       return 'Abril';
//     case 5:
//       return 'Maio';
//     case 6:
//       return 'Junho';
//     case 7:
//       return 'Julho';
//     case 8:
//       return 'Agosto';
//     case 9:
//       return 'Setembro';
//     case 10:
//       return 'Outubro';
//     case 11:
//       return 'Novembro';
//     case 12:
//       return 'Dezembro';
//     default:
//       return '';
//   }
// };

export const REPLACEMONTHNAME = (num) => {
  switch (num) {
    case 1:
      return { short: 'Jan', long: 'Janeiro' };
    case 2:
      return { short: 'Fev', long: 'Fevereiro' };
    case 3:
      return { short: 'Mar', long: 'Março' };
    case 4:
      return { short: 'Abr', long: 'Abril' };
    case 5:
      return { short: 'Mai', long: 'Maio' };
    case 6:
      return { short: 'Jun', long: 'Junho' };
    case 7:
      return { short: 'Jul', long: 'Julho' };
    case 8:
      return { short: 'Ago', long: 'Agosto' };
    case 9:
      return { short: 'Set', long: 'Setembro' };
    case 10:
      return { short: 'Out', long: 'Outubro' };
    case 11:
      return { short: 'Nov', long: 'Novembro' };
    case 12:
      return { short: 'Dez', long: 'Dezembro' };
    default:
      return { short: '', long: '' };
  }
};

// Replace category number with category name
export const REPLACECATEGORYNAME = (num) => {
  switch (num) {
    case 1:
      return 'Transporte';
    case 2:
      return 'Compras Online';
    case 3:
      return 'Saúde e Beleza';
    case 4:
      return 'Serviços Automotivos';
    case 5:
      return 'Restaurantes';
    case 6:
      return 'Supermercados';
    default:
      return '';
  }
};
