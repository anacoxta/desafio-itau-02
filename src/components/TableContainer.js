import React from 'react';
import styled from 'styled-components';

const TableContainer = (props) => {
  return <Div>{props.children}</Div>;
};

const Div = styled.div`
  width: 100%;
  max-width: 900px;
  padding: 0.5rem;
`;

export default TableContainer;
