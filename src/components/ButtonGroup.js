import React, { useContext } from 'react';
import styled from 'styled-components';
import { Context } from '../contexts/Context';

const ButtonGroup = () => {
  const [state, setState] = useContext(Context);

  const btn_allEntries = document.getElementById('btn-allEntries');
  const btn_monthly = document.getElementById('btn-monthly');

  const changeFocus = (newFocus, e) => {
    e.preventDefault();
    if (state.focus !== newFocus) {
      btn_allEntries.classList.toggle('is-outlined');
      btn_monthly.classList.toggle('is-outlined');
      setState({ focus: newFocus });
    }
  };

  return (
    <Buttons className='buttons'>
      <button id='btn-allEntries' className='button is-primary' onClick={(e) => changeFocus('allEntries', e)}>
        Todos os lançamentos
      </button>
      <button id='btn-monthly' className='button is-primary is-outlined' onClick={(e) => changeFocus('monthly', e)}>
        Lançamentos por mês
      </button>
    </Buttons>
  );
};

const Buttons = styled.div`
  margin: 1rem 0.5rem;
`;

export default ButtonGroup;
