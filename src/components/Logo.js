import React from 'react';
import styled from 'styled-components';
import { ReactComponent as Coin } from '../assets/coin.svg';

const Loader = () => {
  return <StyledCoin />;
};

const StyledCoin = styled(Coin)`
  max-height: 2.5rem;
  width: auto;
  margin: 0 0.5rem;
`;

export default Loader;
