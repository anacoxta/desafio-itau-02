import React from 'react';
import styled from 'styled-components';
import Logo from './Logo';

const Navbar = () => {
  return (
    <Nav className='navbar' role='navigation' aria-label='main navigation'>
      <Logo />
      <h3 className='title is-4'>Seus lançamentos</h3>
    </Nav>
  );
};

const Nav = styled.nav`
  width: 100%;
  background-color: #eee;
  display: flex;
  align-items: center;
  height: 3.5rem;

  & .title {
    margin-bottom: 0.4rem;

    @media (min-width: 350px) {
      font-size: 2rem;
    }
  }
`;

export default Navbar;
