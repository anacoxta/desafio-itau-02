import React from 'react';
import styled from 'styled-components';
import { ReactComponent as LoaderIcon } from '../assets/loader.svg';

const Loader = () => {
  return (
    <Div>
      <LoaderIcon />
    </Div>
  );
};

const Div = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export default Loader;
