import React, { useContext } from 'react';
import styled from 'styled-components';
import { Context } from '../contexts/Context';

const FetchError = () => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  return (
    <Div>
      <div className="has-text-danger">ERRO: {state.error.message}</div>
    </Div>
  );
};

const Div = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  & div {
    border: 1px solid #f14668;
    border-radius: .25rem;
    padding: .5rem .8rem;
    margin: 0 .5rem;
  }
`;

export default FetchError;
