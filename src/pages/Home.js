import React, { useContext } from 'react';
import styled from 'styled-components';
import { Context } from '../contexts/Context';
import API from '../services/API';

import Loader from '../components/Loader';
import FetchError from '../components/FetchError';
import Navbar from '../components/Navbar';
import ButtonGroup from '../components/ButtonGroup';
import TableAllEntries from '../components/TableAllEntries';
import TableMonthly from '../components/TableMonthly';

function Home() {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);
  // Calls the API and puts data into the Context
  API('https://desafio-it-server.herokuapp.com/lancamentos');

  return (
    <Div>
      <Navbar />
      <ButtonGroup />
      {state.isLoading && <Loader />}
      {state.error && <FetchError />}
      {!state.error && !state.isLoading && state.focus === 'allEntries' && <TableAllEntries />}
      {!state.error && !state.isLoading && state.focus === 'monthly' && <TableMonthly />}
    </Div>
  );
}

const Div = styled.div`
  width: 100%;
  min-height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default Home;
