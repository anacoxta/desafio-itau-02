import { useEffect, useContext } from 'react';
import { Context } from '../contexts/Context';

/* ==================================================================
1) Calls the context (global state = single source of truth)
2) This specific useEffect hook is called only once, after mounting
3) Fetches from the URL passed
4) THEN, if fetch is successful: parses the response as json
         if fetch is unsuccessful: throws error
5) THEN, sends the response to the context
6) OR CATCHES the error and updates the state
================================================================== */

const API = (url) => {
  const [state, setState] = useContext(Context);
  useEffect(() => {
    fetch(url)
      .then(async res => {
        if (res.ok) {
          return (res = await res.json());
        } else {
          throw Error('Não foi possível carregar os lançamentos');
        }
      })
      .then((data) => {
        setState({ data: data, isLoading: false });
        console.log('state @ API.THEN', state);
      })
      .catch((error) => {
        setState({ error: error, isLoading: false });
        console.log('state @ API.CATCH', state);
      });
  }, []);
};

export default API;
